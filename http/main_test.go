package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"net/http/httptest"
	"testing"
)

func TestIPLimit(t *testing.T) {
	addr := "127.0.0.1:8080"
	r := &http.Request{
		RemoteAddr: addr,
	}

	appendRecoder := httptest.NewRecorder()
	for i := 0; i < 10; i++ {
		buildChain(appendCountHandler, withIpLimitor)(appendRecoder, r)
	}

	// Should Display Error in List
	var resultMap map[string]interface{}
	recoder := httptest.NewRecorder()
	ipListHandler(recoder, r)
	json.Unmarshal(recoder.Body.Bytes(), &resultMap)
	expected := "Error"
	actual := fmt.Sprint(resultMap["127.0.0.1"])
	if expected != actual {
		t.Errorf("The IP should be limit: expected %s,actual %v", expected, actual)
	}
}
