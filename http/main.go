package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"sync"
	"time"
)

var (
	iplimit = map[string]int{}
	mutex   = &sync.Mutex{}
)

func main() {

	fmt.Println("Server Start")
	ticker := time.NewTicker(60 * time.Second)
	go func() {
		for {
			select {
			case <-ticker.C:
				mutex.Lock()
				iplimit = map[string]int{}
				mutex.Unlock()

			}
		}
	}()

	s := &http.Server{
		Addr:           ":8080",
		ReadTimeout:    10 * time.Second,
		WriteTimeout:   10 * time.Second,
		MaxHeaderBytes: 1 << 20,
	}

	// Regest path and interceptor
	http.HandleFunc("/append_count", buildChain(appendCountHandler, withIpLimitor))
	http.HandleFunc("/list", buildChain(ipListHandler))
	log.Fatal(s.ListenAndServe())
}

func appendCountHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Add Count \n")
}

// The list only for display ips
type ipListDisplayList map[string]interface{}

const limit = 60

func ipListHandler(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	list := make(ipListDisplayList)
	for ip, count := range iplimit {
		if count > limit {
			list[ip] = "Error"
		} else {
			list[ip] = count
		}
	}
	i, _ := json.Marshal(list)
	fmt.Fprintf(w, string(i))
}

type middleware func(http.HandlerFunc) http.HandlerFunc

func buildChain(f http.HandlerFunc, m ...middleware) http.HandlerFunc {
	// if our chain is done, use the original handlerfunc
	if len(m) == 0 {
		return f
	}
	// otherwise nest the handlerfuncs
	return m[0](buildChain(f, m[1:cap(m)]...))
}

func withIpLimitor(f http.HandlerFunc) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		host, _, _ := net.SplitHostPort(r.RemoteAddr)
		if _, ok := iplimit[host]; ok {
			iplimit[host] += 1
		} else {
			iplimit[host] = 1
		}

		if iplimit[host] > limit {
			fmt.Fprintf(w, "Error")
			return
		}

		f(w, r)
	}
}
