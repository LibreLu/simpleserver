# Simple Server
## Product Requiement
1. Create a simple server can limit 60 request counts for each IP
2. The limit IP list should be reset every 1 min
3. The server should returns error when current IP reach the limit
4. The Error should display in IP limit list

## Solution
I am not using any framework such as Gin to build up the server. I think I should use in library Http package to handle the request. This is what I done:

1. A middleware pattern to append the request count
2. Using Mutext to avoid the race condition when reset the limit IP list
3. Using intergration test to test the limit IP logic

## Getting start
Clone the repo, type command `go run http/main.go` in termianl. This will start up a Http Server.

### Endpoints
|URL|Method|Response|Despcription|
|----|-----|-------|------------|
|/append_count|GET||Append the count to count list with current IP|
|/list|GET|{`IP`:`Count`}|Display IP List|
